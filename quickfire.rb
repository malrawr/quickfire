#!/usr/bin/ruby

require 'mkmf'

# Make the MakeMakefile logger write file output to null.
# This is the ruby equivalnt of /dev/null
# Probably requires ruby >= 1.9.3
# https://gist.github.com/mnem/2540fece4ed9d3403b98
module MakeMakefile::Logging
  @logfile = File::NULL
end

TOP_TCP_PORTS = "1,3-4,6-7,9,13,17,19-26,30,32-33,37,42-43,49,53,70,79-85,88-90,99-100,106,109-111,113,119,125,135,139,143-144,146,161,163,179,199,211-212,222,254-256,259,264,280,301,306,311,340,366,389,406-407,416-417,425,427,443-445,458,464-465,481,497,500,512-515,524,541,543-545,548,554-555,563,587,593,616-617,625,631,636,646,648,666-668,683,687,691,700,705,711,714,720,722,726,749,765,777,783,787,800-801,808,843,873,880,888,898,900-903,911-912,981,987,990,992-993,995,999-1002,1007,1009-1011,1021-1100,1102,1104-1108,1110-1114,1117,1119,1121-1124,1126,1130-1132,1137-1138,1141,1145,1147-1149,1151-1152,1154,1163-1166,1169,1174-1175,1183,1185-1187,1192,1198-1199,1201,1213,1216-1218,1233-1234,1236,1244,1247-1248,1259,1271-1272,1277,1287,1296,1300-1301,1309-1311,1322,1328,1334,1352,1417,1433-1434,1443,1455,1461,1494,1500-1501,1503,1521,1524,1533,1556,1580,1583,1594,1600,1641,1658,1666,1687-1688,1700,1717-1721,1723,1755,1761,1782-1783,1801,1805,1812,1839-1840,1862-1864,1875,1900,1914,1935,1947,1971-1972,1974,1984,1998-2010,2013,2020-2022,2030,2033-2035,2038,2040-2043,2045-2049,2065,2068,2099-2100,2103,2105-2107,2111,2119,2121,2126,2135,2144,2160-2161,2170,2179,2190-2191,2196,2200,2222,2251,2260,2288,2301,2323,2366,2381-2383,2393-2394,2399,2401,2492,2500,2522,2525,2557,2601-2602,2604-2605,2607-2608,2638,2701-2702,2710,2717-2718,2725,2800,2809,2811,2869,2875,2909-2910,2920,2967-2968,2998,3000-3001,3003,3005-3007,3011,3013,3017,3030-3031,3052,3071,3077,3128,3168,3211,3221,3260-3261,3268-3269,3283,3300-3301,3306,3322-3325,3333,3351,3367,3369-3372,3389-3390,3404,3476,3493,3517,3527,3546,3551,3580,3659,3689-3690,3703,3737,3766,3784,3800-3801,3809,3814,3826-3828,3851,3869,3871,3878,3880,3889,3905,3914,3918,3920,3945,3971,3986,3995,3998,4000-4006,4045,4111,4125-4126,4129,4224,4242,4279,4321,4343,4443-4446,4449,4550,4567,4662,4848,4899-4900,4998,5000-5004,5009,5030,5033,5050-5051,5054,5060-5061,5080,5087,5100-5102,5120,5190,5200,5214,5221-5222,5225-5226,5269,5280,5298,5357,5405,5414,5431-5432,5440,5500,5510,5544,5550,5555,5560,5566,5631,5633,5666,5678-5679,5718,5730,5800-5802,5810-5811,5815,5822,5825,5850,5859,5862,5877,5900-5904,5906-5907,5910-5911,5915,5922,5925,5950,5952,5959-5963,5987-5989,5998-6007,6009,6025,6059,6100-6101,6106,6112,6123,6129,6156,6346,6389,6502,6510,6543,6547,6565-6567,6580,6646,6666-6669,6689,6692,6699,6779,6788-6789,6792,6839,6881,6901,6969,7000-7002,7004,7007,7019,7025,7070,7100,7103,7106,7200-7201,7402,7435,7443,7496,7512,7625,7627,7676,7741,7777-7778,7800,7911,7920-7921,7937-7938,7999-8002,8007-8011,8021-8022,8031,8042,8045,8080-8090,8093,8099-8100,8180-8181,8192-8194,8200,8222,8254,8290-8292,8300,8333,8383,8400,8402,8443,8500,8600,8649,8651-8652,8654,8701,8800,8873,8888,8899,8994,9000-9003,9009-9011,9040,9050,9071,9080-9081,9090-9091,9099-9103,9110-9111,9200,9207,9220,9290,9415,9418,9485,9500,9502-9503,9535,9575,9593-9595,9618,9666,9876-9878,9898,9900,9917,9929,9943-9944,9968,9998-10004,10009-10010,10012,10024-10025,10082,10180,10215,10243,10566,10616-10617,10621,10626,10628-10629,10778,11110-11111,11967,12000,12174,12265,12345,13456,13722,13782-13783,14000,14238,14441-14442,15000,15002-15004,15660,15742,16000-16001,16012,16016,16018,16080,16113,16992-16993,17877,17988,18040,18101,18988,19101,19283,19315,19350,19780,19801,19842,20000,20005,20031,20221-20222,20828,21571,22939,23502,24444,24800,25734-25735,26214,27000,27352-27353,27355-27356,27715,28201,30000,30718,30951,31038,31337,32768-32785,33354,33899,34571-34573,35500,38292,40193,40911,41511,42510,44176,44442-44443,44501,45100,48080,49152-49161,49163,49165,49167,49175-49176,49400,49999-50003,50006,50300,50389,50500,50636,50800,51103,51493,52673,52822,52848,52869,54045,54328,55055-55056,55555,55600,56737-56738,57294,57797,58080,60020,60443,61532,61900,62078,63331,64623,64680,65000,65129,65389"

TOP_UDP_PORTS = "U:2-3,U:7,U:9,U:13,U:17,U:19-23,U:37-38,U:42,U:49,U:53,U:67-69,U:80,U:88,U:111-113,U:120,U:123,U:135-139,U:158,U:161-162,U:177,U:192,U:199,U:207,U:217,U:363,U:389,U:402,U:407,U:427,U:434,U:443,U:445,U:464,U:497,U:500,U:502,U:512-515,U:517-518,U:520,U:539,U:559,U:593,U:623,U:626,U:631,U:639,U:643,U:657,U:664,U:682-689,U:764,U:767,U:772-776,U:780-782,U:786,U:789,U:800,U:814,U:826,U:829,U:838,U:902-903,U:944,U:959,U:965,U:983,U:989-990,U:996-1001,U:1007-1008,U:1012-1014,U:1019-1051,U:1053-1060,U:1064-1070,U:1072,U:1080-1081,U:1087-1088,U:1090,U:1100-1101,U:1105,U:1124,U:1200,U:1214,U:1234,U:1346,U:1419,U:1433-1434,U:1455,U:1457,U:1484-1485,U:1524,U:1645-1646,U:1701,U:1718-1719,U:1761,U:1782,U:1804,U:1812-1813,U:1885-1886,U:1900-1901,U:1993,U:2000,U:2002,U:2048-2049,U:2051,U:2148,U:2160-2161,U:2222-2223,U:2343,U:2345,U:2362,U:2967,U:3052,U:3130,U:3283,U:3296,U:3343,U:3389,U:3401,U:3456-3457,U:3659,U:3664,U:3702-3703,U:4000,U:4008,U:4045,U:4444,U:4500,U:4666,U:4672,U:5000-5003,U:5010,U:5050,U:5060,U:5093,U:5351,U:5353,U:5355,U:5500,U:5555,U:5632,U:6000-6002,U:6004,U:6050,U:6346-6347,U:6970-6971,U:7000,U:7938,U:8000-8001,U:8010,U:8181,U:8193,U:8900,U:9000-9001,U:9020,U:9103,U:9199-9200,U:9370,U:9876-9877,U:9950,U:10000,U:10080,U:11487,U:16086,U:16402,U:16420,U:16430,U:16433,U:16449,U:16498,U:16503,U:16545,U:16548,U:16573,U:16674,U:16680,U:16697,U:16700,U:16708,U:16711,U:16739,U:16766,U:16779,U:16786,U:16816,U:16829,U:16832,U:16838-16839,U:16862,U:16896,U:16912,U:16918-16919,U:16938-16939,U:16947-16948,U:16970,U:16972,U:16974,U:17006,U:17018,U:17077,U:17091,U:17101,U:17146,U:17184-17185,U:17205,U:17207,U:17219,U:17236-17237,U:17282,U:17302,U:17321,U:17331-17332,U:17338,U:17359,U:17417,U:17423-17424,U:17455,U:17459,U:17468,U:17487,U:17490,U:17494,U:17505,U:17533,U:17549,U:17573,U:17580,U:17585,U:17592,U:17605,U:17615-17616,U:17629,U:17638,U:17663,U:17673-17674,U:17683,U:17726,U:17754,U:17762,U:17787,U:17814,U:17823-17824,U:17836,U:17845,U:17888,U:17939,U:17946,U:17989,U:18004,U:18081,U:18113,U:18134,U:18156,U:18228,U:18234,U:18250,U:18255,U:18258,U:18319,U:18331,U:18360,U:18373,U:18449,U:18485,U:18543,U:18582,U:18605,U:18617,U:18666,U:18669,U:18676,U:18683,U:18807,U:18818,U:18821,U:18830,U:18832,U:18835,U:18869,U:18883,U:18888,U:18958,U:18980,U:18985,U:18987,U:18991,U:18994,U:18996,U:19017,U:19022,U:19039,U:19047,U:19075,U:19096,U:19120,U:19130,U:19140-19141,U:19154,U:19161,U:19165,U:19181,U:19193,U:19197,U:19222,U:19227,U:19273,U:19283,U:19294,U:19315,U:19322,U:19332,U:19374,U:19415,U:19482,U:19489,U:19500,U:19503-19504,U:19541,U:19600,U:19605,U:19616,U:19624-19625,U:19632,U:19639,U:19647,U:19650,U:19660,U:19662-19663,U:19682-19683,U:19687,U:19695,U:19707,U:19717-19719,U:19722,U:19728,U:19789,U:19792,U:19933,U:19935-19936,U:19956,U:19995,U:19998,U:20003-20004,U:20019,U:20031,U:20082,U:20117,U:20120,U:20126,U:20129,U:20146,U:20154,U:20164,U:20206,U:20217,U:20249,U:20262,U:20279,U:20288,U:20309,U:20313,U:20326,U:20359-20360,U:20366,U:20380,U:20389,U:20409,U:20411,U:20423-20425,U:20445,U:20449,U:20464-20465,U:20518,U:20522,U:20525,U:20540,U:20560,U:20665,U:20678-20679,U:20710,U:20717,U:20742,U:20752,U:20762,U:20791,U:20817,U:20842,U:20848,U:20851,U:20865,U:20872,U:20876,U:20884,U:20919,U:21000,U:21016,U:21060,U:21083,U:21104,U:21111,U:21131,U:21167,U:21186,U:21206-21207,U:21212,U:21247,U:21261,U:21282,U:21298,U:21303,U:21318,U:21320,U:21333,U:21344,U:21354,U:21358,U:21360,U:21364,U:21366,U:21383,U:21405,U:21454,U:21468,U:21476,U:21514,U:21524-21525,U:21556,U:21566,U:21568,U:21576,U:21609,U:21621,U:21625,U:21644,U:21649,U:21655,U:21663,U:21674,U:21698,U:21702,U:21710,U:21742,U:21780,U:21784,U:21800,U:21803,U:21834,U:21842,U:21847,U:21868,U:21898,U:21902,U:21923,U:21948,U:21967,U:22029,U:22043,U:22045,U:22053,U:22055,U:22105,U:22109,U:22123-22124,U:22341,U:22692,U:22695,U:22739,U:22799,U:22846,U:22914,U:22986,U:22996,U:23040,U:23176,U:23354,U:23531,U:23557,U:23608,U:23679,U:23781,U:23965,U:23980,U:24007,U:24279,U:24511,U:24594,U:24606,U:24644,U:24854,U:24910,U:25003,U:25157,U:25240,U:25280,U:25337,U:25375,U:25462,U:25541,U:25546,U:25709,U:25931,U:26407,U:26415,U:26720,U:26872,U:26966,U:27015,U:27195,U:27444,U:27473,U:27482,U:27707,U:27892,U:27899,U:28122,U:28369,U:28465,U:28493,U:28543,U:28547,U:28641,U:28840,U:28973,U:29078,U:29243,U:29256,U:29810,U:29823,U:29977,U:30263,U:30303,U:30365,U:30544,U:30656,U:30697,U:30704,U:30718,U:30975,U:31059,U:31073,U:31109,U:31189,U:31195,U:31335,U:31337,U:31365,U:31625,U:31681,U:31731,U:31891,U:32345,U:32385,U:32528,U:32768-32780,U:32798,U:32815,U:32818,U:32931,U:33030,U:33249,U:33281,U:33354-33355,U:33459,U:33717,U:33744,U:33866,U:33872,U:34038,U:34079,U:34125,U:34358,U:34422,U:34433,U:34555,U:34570,U:34577-34580,U:34758,U:34796,U:34855,U:34861-34862,U:34892,U:35438,U:35702,U:35777,U:35794,U:36108,U:36206,U:36384,U:36458,U:36489,U:36669,U:36778,U:36893,U:36945,U:37144,U:37212,U:37393,U:37444,U:37602,U:37761,U:37783,U:37813,U:37843,U:38037,U:38063,U:38293,U:38412,U:38498,U:38615,U:39213,U:39217,U:39632,U:39683,U:39714,U:39723,U:39888,U:40019,U:40116,U:40441,U:40539,U:40622,U:40708,U:40711,U:40724,U:40732,U:40805,U:40847,U:40866,U:40915,U:41058,U:41081,U:41308,U:41370,U:41446,U:41524,U:41638,U:41702,U:41774,U:41896,U:41967,U:41971,U:42056,U:42172,U:42313,U:42431,U:42434,U:42508,U:42557,U:42577,U:42627,U:42639,U:43094,U:43195,U:43370,U:43514,U:43686,U:43824,U:43967,U:44101,U:44160,U:44179,U:44185,U:44190,U:44253,U:44334,U:44508,U:44923,U:44946,U:44968,U:45247,U:45380,U:45441,U:45685,U:45722,U:45818,U:45928,U:46093,U:46532,U:46836,U:47624,U:47765,U:47772,U:47808,U:47915,U:47981,U:48078,U:48189,U:48255,U:48455,U:48489,U:48761,U:49152-49163,U:49165-49182,U:49184-49202,U:49204-49205,U:49207-49216,U:49220,U:49222,U:49226,U:49259,U:49262,U:49306,U:49350,U:49360,U:49393,U:49396,U:49503,U:49640,U:49968,U:50099,U:50164,U:50497,U:50612,U:50708,U:50919,U:51255,U:51456,U:51554,U:51586,U:51690,U:51717,U:51905,U:51972,U:52144,U:52225,U:52503,U:53006,U:53037,U:53571,U:53589,U:53838,U:54094,U:54114,U:54281,U:54321,U:54711,U:54807,U:54925,U:55043,U:55544,U:55587,U:56141,U:57172,U:57409-57410,U:57813,U:57843,U:57958,U:57977,U:58002,U:58075,U:58178,U:58419,U:58631,U:58640,U:58797,U:59193,U:59207,U:59765,U:59846,U:60172,U:60381,U:60423,U:61024,U:61142,U:61319,U:61322,U:61370,U:61412,U:61481,U:61550,U:61685,U:61961,U:62154,U:62287,U:62575,U:62677,U:62699,U:62958,U:63420,U:63555,U:64080,U:64481,U:64513,U:64590,U:64727,U:65024"

FULL_TCP_PORTS = "0-65535"

FULL_UDP_PORTS = "U:0-65535"

# Checks to see if applications exist
# Note: find_executable0 method does not pipe to STDOUT
def application_check
  array = ['nmap', 'masscan', 'amap']
  array.each do |program| 
    unless find_executable0(program) 
      # Writes message to STDERR and exit with an error status code
      raise "Unable to find #{program}! Install it and make sure it's in your PATH environment!" 
    end
  end
end

def root_check
  raise "This script must run as root!" unless Process.uid == 0
end

def full_tcp
  puts "Obtaining open TCP ports using masscan..."
  puts "[+] masscan -e #{ARGS[:interface]} -p0-65535 --max-rate #{ARGS[:rate]} #{ARGS[:target]}"
  system("script", "-c", "masscan -e #{ARGS[:interface]} -p0-65535 --max-rate #{ARGS[:rate]} #{ARGS[:target]} > full_tcp.scan", "masscan.log", :out => File::NULL)
  system("cat full_tcp.scan")
  nmap_ports = `cat full_tcp.scan | grep Discovered | cut -d " " -f4- | cut -d "/" -f1 | tr '\n' ','`
  unless nmap_ports.nil? || nmap_ports.empty?
    puts "[+] nmap -e #{ARGS[:interface]} -sC -sV #{ARGS[:target]} -p #{nmap_ports}"
    system("nmap -e #{ARGS[:interface]} -sC -sV #{ARGS[:target]} -p #{nmap_ports}")
  else
    clean
    raise ("#{$0}: No TCP ports found")
  end
  if ARGS[:amap] then amap_tcp end
end

def full_tcp_fix(tcp_ports)
  if tcp_ports == TOP_TCP_PORTS then puts "[*] Running top 1000 TCP port scan using masscan..." end
  if tcp_ports == FULL_TCP_PORTS then puts "[*] Running full TCP port scan using masscan..." end
  #puts "[+] masscan -e #{ARGS[:interface]} -p#{tcp_ports} --max-rate #{ARGS[:rate]} #{ARGS[:target]} -v"
  system("script --return -c \"masscan -e #{ARGS[:interface]} -p#{tcp_ports} --max-rate #{ARGS[:rate]} #{ARGS[:target]} -v | tee full_tcp.scan \" masscan.log | grep -q \"xmit: stopping transmit thread\"", :out => File::NULL)
  system("stty sane") # Fix terminal output glitch
  system("cat full_tcp.scan")
  nmap_ports = `cat full_tcp.scan | grep Discovered | cut -d " " -f4- | cut -d "/" -f1 | tr '\n' ','`
  unless nmap_ports.nil? || nmap_ports.empty?
    puts "[+] nmap -e #{ARGS[:interface]} -Pn -n -sC -sV #{ARGS[:target]} -p #{nmap_ports}"
    system("nmap -e #{ARGS[:interface]} -Pn -n -sC -sV #{ARGS[:target]} -p #{nmap_ports}")
  else
    clean
    raise ("#{$0}: No TCP ports found")
  end 
  if ARGS[:amap] then amap_tcp end 
end

def amap_tcp
  amap_ports = `cat full_tcp.scan | grep Discovered | cut -d " " -f4- | cut -d "/" -f1`.split("\n")
  unless amap_ports.nil? || amap_ports.empty?
    amap_ports.each do |x|
      puts "[+] amap -Adbqv -1 -b #{ARGS[:target]} #{x}"
      system("amap -Adbqv -1 -b #{ARGS[:target]} #{x} | tail -n +9 | sed '$d'")
    end
  else
    clean
    raise ("#{$0}: No TCP ports found")
  end
end

def full_udp
  puts "Obtaining open UDP ports using masscan..."
  puts "[+] masscan -e #{ARGS[:interface]} -pU:0-65535 --max-rate #{ARGS[:rate]} #{ARGS[:target]}"
  system("script", "-c", "masscan -e #{ARGS[:interface]} -pU:0-65535 --max-rate #{ARGS[:rate]} #{ARGS[:target]} > full_udp.scan", "masscan.log", :out => File::NULL)
  system("cat full_udp.scan")
  nmap_ports = `cat full_udp.scan | grep Discovered | cut -d " " -f4- | cut -d "/" -f1 | tr '\n' ','`
  unless nmap_ports.nil? || nmap_ports.empty?
    puts "[+] nmap -e #{ARGS[:interface]} -sC -sV #{ARGS[:target]} -p #{nmap_ports}"
    system("nmap -e #{ARGS[:interface]} -sC -sV #{ARGS[:target]} -p #{nmap_ports}")
  else
    clean
    raise ("#{$0}: No UDP ports found")
  end
  if ARGS[:amap] then amap_udp end
end

def full_udp_fix(udp_ports)
  if udp_ports == TOP_UDP_PORTS then puts "[*] Running top 1000 UDP port scan using masscan..." end
  if udp_ports == FULL_UDP_PORTS then puts "[*] Running full UDP port scan using masscan..." end
  #puts "[+] masscan -e #{ARGS[:interface]} -p #{udp_ports} --max-rate #{ARGS[:rate]} #{ARGS[:target]}"
  system("script --return -c \"masscan -e #{ARGS[:interface]} -p #{udp_ports} --max-rate #{ARGS[:rate]} #{ARGS[:target]} -v | tee full_udp.scan \" masscan.log | grep -q \"xmit: stopping transmit thread\"", :out => File::NULL)
  system("stty sane") # Fix terminal output glitch
  system("cat full_udp.scan")
  nmap_ports = `cat full_udp.scan | grep Discovered | cut -d " " -f4- | cut -d "/" -f1 | tr '\n' ','`
  unless nmap_ports.nil? || nmap_ports.empty?
    puts "[+] nmap -e #{ARGS[:interface]} -Pn -n -sU -sC -sV #{ARGS[:target]} -p #{nmap_ports}"
    system("nmap -e #{ARGS[:interface]} -Pn -n -sU -sC -sV #{ARGS[:target]} -p #{nmap_ports}")
  else
    clean
    raise ("#{$0}: No UDP ports found")
  end
  if ARGS[:amap] then amap_udp end
end


def amap_udp 
  amap_ports = `cat full_udp.scan | grep Discovered | cut -d " " -f4- | cut -d "/" -f1`.split("\n")
  unless amap_ports.nil? || amap_ports.empty?
    amap_ports.each do |x|
      puts "[+] amap -Adbqv -1 -b #{ARGS[:target]} -u #{x}"
      system("amap -Adbqv -1 -b #{ARGS[:target]} -u #{x} | tail -n +9 | sed '$d'")
    end
  else
    clean
    raise ("#{$0}: No UDP ports found")
  end
end

def clean
  file_array = ["masscan.log", "full_tcp.scan", "full_udp.scan", "paused.conf"]
  file_array.each { |file| system("rm #{file}", :out => File::NULL, :err => File::NULL) }
end

VERSION = "0.1"

BANNER = <<ENDBANNER

  \033[1mQ U I C K F I R E\033[0m

ENDBANNER

USAGE = <<ENDUSAGE
Usage: #{$0} -t target [-i interface] [-r rate] [-sT [-pT standard/all]] [-sU [-pU standard/all]] [-Pn] [-a] [-h] [-b]
ENDUSAGE

HELP = <<ENDHELP
  -h, --help       Show this help.
  -v, --version    Show the version number (#{VERSION}).
  -b, --banner     Show banner
  -t, --target     Target IP Address. This option is required.
  -i, --interface  Network interface, optional.  
  -r, --rate       Max rate scanning speed.
                   [Defaults to 350] 
  -sT,--scan-tcp   Toggle TCP Scan
                   [Defaults to True]
  -pT,--port-tcp   Scan Top 1000 Ports (standard) or all ports (all)
                   [Defaults to standard]
  -sU,--scan-udp   Toggle UDP Scan
  -pU,--port-udp   Scan Top 1000 Ports (standard) or all ports (all)
                   [Defaults to standard]
  -Pn,--ping-no    Treat host as online, skips initial ping
  -a, --amap       Specify for AMAP enumeration. Displays to STDOUT.
ENDHELP

ARGS = { :port_tcp => 'standard', :port_udp => 'standard', :rate => '350' } # Setting default values
UNFLAGGED_ARGS = []
next_arg = UNFLAGGED_ARGS.first
ARGV.each do |arg|
  case arg
    when '-h','--help'      then ARGS[:help]      = true
    when '-v','--version'   then ARGS[:version]   = true
    when '-b','--banner'    then ARGS[:banner]    = true
    when '-t','--target'    then next_arg = :target
    when '-i','--interface' then next_arg = :interface
    when '-r','--rate'      then next_arg = :rate
    when '-p','--protocol'  then next_arg = :protocol
    when '-sT','--scan-tcp' then ARGS[:scan_tcp]  = true
    when '-pT','--port-tcp' then next_arg = :port_tcp
    when '-sU','--scan-udp' then ARGS[:scan_udp]  = true
    when '-pU','--port-udp' then next_arg = :port_udp
    when '-Pn','--ping-no'  then ARGS[:ping_no]   = true    
    when '-a','--amap'      then ARGS[:amap]      = true
    else
      if next_arg
        ARGS[next_arg] = arg
        UNFLAGGED_ARGS.delete( next_arg )
      end
      next_arg = UNFLAGGED_ARGS.first
    end
end

if !ARGS[:scan_tcp] && !ARGS[:scan_udp] then ARGS[:scan_tcp] = true end

start = Time.now

#puts "#{$0} v#{VERSION}";exit if ARGS[:version]

if ARGS[:banner]
  puts BANNER
end

if ARGS[:help] || !ARGS[:target]#or !ARGS[:directory]
  puts USAGE unless ARGS[:version]
  puts HELP if ARGS[:help]
  exit
end

unless ARGS[:ping_no]
  unless system("ping -c 1 #{ARGS[:target]}", :out => File::NULL) then raise "#{$0}: #{ARGS[:target]} unreachable" end
else
  puts "Skipping host discovery"
end

unless ARGS[:interface] then ARGS[:interface] = `ip route get #{ARGS[:target]} | sed 's/.*dev/dev/' | awk '{print $2}' | tr -d '\n'` end

if ARGS[:interface]
  iface_array = `netstat -i | tail +3 | awk '{print $1}'`.split("\n")
  unless iface_array.include?(ARGS[:interface]) 
      raise "#{$0}: unrecognized interface '#{ARGS[:interface]}'"
  end
end

if ARGS[:scan_tcp]
  valid_options = ["standard", "all"]
  unless valid_options.include?(ARGS[:port_tcp])
    raise "#{$0}: unrecognized option '#{ARGS[:port_tcp]}'"
  end
  if ARGS[:port_tcp] == "all" then ARGS[:port_tcp] = FULL_TCP_PORTS end
  if ARGS[:port_tcp] == "standard" then ARGS[:port_tcp] = TOP_TCP_PORTS end
end

if ARGS[:scan_udp]
  valid_options = ["standard", "all"]
  unless valid_options.include?(ARGS[:port_udp])
    raise "#{$0}: unrecognized option '#{ARGS[:port_udp]}'"
  end
  if ARGS[:port_udp] == "all" then ARGS[:port_udp] = FULL_UDP_PORTS end
  if ARGS[:port_udp] == "standard" then ARGS[:port_udp] = TOP_UDP_PORTS end
end

trap("SIGINT") do
  puts "WARNING! CTRL + C Detected, Shutting things down and exiting program...."
  clean
  exit 666
end

root_check
application_check

puts "Target:'#{ARGS[:target]}', Interface: '#{ARGS[:interface]}', Rate: '#{ARGS[:rate]}'"
full_tcp_fix(ARGS[:port_tcp]) if ARGS[:scan_tcp]
full_udp_fix(ARGS[:port_udp]) if ARGS[:scan_udp]

clean

puts "[+] Total scan time was #{(Time.now - start).round(2)} seconds"
