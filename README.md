# Quickfire

Nmap is the most powerful port scanner, however one of it's downsides is speed. Normally this isn't a problem if you stick to the default nmap ports. However often times it is necessary to do a full scan of every port, something that would take nmap ages to do, even more so with certain VPN connections. This is where masscan comes in, it is the quickest port scanner out there. Using these two tools together we can create a comprehensive scan of all 65,535 ports complete with nmap's NSE scripts.

Another tool that is very useful for initial enumeration, is amap. It can identify protocols that nmap can't. It also has the added benefit of seeing information on all ports such as banner, or website headers.

These three tools working together makes up quickfire, the routine is as follows:

masscan => nmap => amap

masscan
  * Scans all 65,535 ports
  * Returns all open ports

nmap
  * Nmap aggressively scans the open ports

amap
  * Returns output for all ports found 

## Updates

Added auto routing
  * The script can now determine the interface used against each target. 
  * Interface flag no longer needs to be specified

Added ping check
  * The script now checks if the host is up, if it is unreachable then an exception occurs
  * 

## ISSUES

* Masscan [FIXED]
  * Sometimes fails to work while using tun0, there may be a work around to this
    * My work around if to use `script` and read the log file that it creates, do a realtime grep for the final string and force close the scanner.

## Planned Features

* TODO
  * Create an amap output log file
  * Create a flag that let's you skip the ping check [COMPLETE]
